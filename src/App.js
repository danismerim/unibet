import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Matches from "./components/Matches";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/">
          <Matches />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
