import React, { useState, useEffect } from "react";
import jsonp from 'jsonp';

function Matches() {
    const [currentMatches, setCurrentMatches] = useState([]);
    const [currentIndex, setCurrentIndex] = useState(0);
    const [isError, setIsError] = useState(false);
    const storageKey = "matches";
    const sportIcons = [
        {
            name: "FOOTBALL",
            icon: "/images/icons/football.png"
        },
        {
            name: "TENNIS",
            icon: "/images/icons/tennis.png"
        }, {
            name: "BASKETBALL",
            icon: "/images/icons/basketball.png"
        }
    ];

    useEffect(() => {
        const cachedMatches = JSON.parse(localStorage.getItem(storageKey));
        if (!cachedMatches || !cachedMatches.events || isStorageExpired(cachedMatches.createdAt)) {
            requestMatches();
        } else {
            setCurrentMatches(cachedMatches.events)
        }
    }, []);

    useEffect(() => {
        const interval = setInterval(nextMatch, 3000);
        return () => clearInterval(interval);
    }, [currentMatches, currentIndex]);

    const isStorageExpired = (storageDate) => {
        const savedAt = new Date(storageDate);
        const currentDate = new Date();
        const diffDates = currentDate - savedAt;

        return ((diffDates / 1000) / 60) > 2; // expires in 2 minutes
    }

    const requestMatches = async () => {
        jsonp("/v1/feeds/sportsbook/event/live.jsonp?app_id=ca7871d7&app_key=5371c125b8d99c8f6b5ff9a12de8b85a",
            { headers: { 'Content-Type': 'application/json' }, method: "GET" }, (error, data) => {
                localStorage.setItem(storageKey, JSON.stringify({ createdAt: new Date, events: data.liveEvents }));
                if (!error) {
                    setCurrentMatches(data.liveEvents)
                } else {
                    setIsError(true)
                    setCurrentMatches([]);
                }
            });
    }

    const nextMatch = () => {
        if (currentIndex < (currentMatches.length - 1)) {
            const newIndex = currentIndex + 1;
            setCurrentIndex(newIndex);
        } else {
            setCurrentIndex(0);
        }
    }

    const setDisplayedDate = (date) => {
        let startDate = new Date(date);
        let todaysDate = new Date();
        if (startDate.toISOString().split('T')[0] === todaysDate.toISOString().split('T')[0]) {
            return `Today,  ${startDate.getHours()}:${startDate.getMinutes()}`;
        }
        return startDate.toISOString().split('T')[0]
    }

    const getIcon = (sport) => {
        const specialIcon = sportIcons.find(s => s.name == sport);
        return specialIcon ? specialIcon.icon : "/images/icons/default.png"
    }


    return (
        <div id="container">
            <header>
                <div id="logo" className="ml-1">Unibet</div>
            </header>
            <div id="content">
                <article className="row">
                    <div className="row">
                        <h1>Live matches</h1>
                        <p className="preamble">
                            Here is a list of matches that are live right now.
                        </p>
                    </div>
                    <div className="col-lg-9 col-md-8 col-sm-12">

                        <div id="live-matches">
                            {isError &&
                                <>
                                    <div className="score">Unavailable service.</div>
                                    <div className="score">Please try again in a few moments!</div>
                                </>
                            }
                            {!isError && currentMatches && currentMatches.map((match, i) => (
                                <div className={i === currentIndex ? "active" : "inactive"} key={i}>
                                    <div className="score">{match.liveData && match.liveData.score ? match.liveData.score.home : "?"} - {match.liveData && match.liveData.score ? match.liveData.score.away : "?"}</div>
                                    <div className="team-names d-flex align-items-center justify-content-center">
                                        <img src={getIcon(match.event.sport)} />
                                        <div>{match.event.homeName} - {match.event.awayName}</div>
                                    </div>
                                    <div className="start-time">{setDisplayedDate(match.event.start)} </div>
                                    <button type="button" className="btn"><a href={"https://www.unibet.com/betting#/event/live/" + match.event.id} target="_blank">Place a bet</a></button>
                                </div>
                            ))}
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-sm-12 betting-info mt-4 mt-md-0">
                        <p className="title">Live betting</p>
                        <p>Place your bets as the action unfolds. We offer a wide selection of live betting events and you can place both single and combination bets.</p>
                        <p>You will be able to see an in-play scoreboard with the current result and match stats, while on selected events you will also be able to watch the action live with Unibet TV on the desktop site.</p>
                    </div>
                </article>
            </div>
            <footer>
                <div className="inner">
                    <p>&copy; 1997-2015, Unibet. All rights reserved.</p>
                </div>
            </footer>
        </div>
    );
}

export default Matches;